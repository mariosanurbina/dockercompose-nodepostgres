const express = require('express');
const path =require('path');



//Initializations
const app=express();
require('./database');

//Settings
app.set('port', process.env.PORT || 8080);
app.get('/', function (req, res) {
    res.send('Hello World!');
  });

//Static Files
app.use(express.static(path.join(__dirname,'public')));
//Server Listening

app.listen(app.get('port'), function () {
  console.log('Example app listening on port', app.get('port'));
});
