const { Client } = require('pg');

const connectionData = {
  user: 'admin',
  host: 'dbpostgres',
  database: 'appdb',
  password: 'adminPassword',
  port: 5432,
}
const client = new Client(connectionData)

client.connect()
  .then(() => {
        
    console.log("Connected successfully")
})
.catch(err => {
  console.log(err);
})

client.query("CREATE TABLE customers (name VARCHAR(255), address VARCHAR(255))")
    .then(response => {
        
        client.end()
    })
    .catch(err => {
        client.end()
    })